<?php
//string means a set of many characters

//single quated example

$value= 100;
$str1= 'I am single quated $value. i show everything under single qutation <br>';
echo $str1; // single quota will print everything under it

// double quated value

$str2= "<br> I am double quated $value.   I show everything but when I get variable i replace with its value <br>";
echo $str2; //double quated will print everything without variable. it will replace by its value.


// Heredoc syntax
   // its equvalent to double quated. a big set will sent by this

$var= 1000;

$heredocstr1=<<<BITM

<br> I am heredoc. big data sent by me. so try me $var <br>

BITM;

echo $heredocstr1;


 // nowdoc syntax
// its equvalen to single quated. And take care about space. no space reqrd in string.

$nowdocstr1=<<<'BITM'

<br> I am heredoc. big data sent by me. so try me $var

BITM;

echo $nowdocstr1;

?>


