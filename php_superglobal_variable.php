<?php
//super global variable can be access from everywhere.

//GLOBALS

$x = 75;

function doSomething(){
    $x = 25;
    echo "local x = $x ";
    echo "global x = " . $GLOBALS['x']. "<br>";
}
doSomething();

//_SERVER

echo $_SERVER['PHP_SELF'];
echo "<br>";
echo $_SERVER['SERVER_NAME'] . "<br>";
echo $_SERVER['HTTP_REFERER'] . "<br>"; // refer holo kon page theke elam. test.file er je php file eta okhan theke run hobe.
echo $_SERVER['SCRIPT_FILENAME'] . "<br>";
?>